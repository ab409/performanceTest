package test;
/** 
 * @author: yuguangyuan
 * @date 创建时间：2015年8月4日 下午5:41:05   
 */
public class HeapSort {
	
	private void fixdown(Integer[] array, int length, int i){
		int j = i;
		while(j * 2 + 1 < length){
			int k = j * 2 + 1, t = j * 2 + 2;
			if(t < length && array[t] > array[k])
				k = t;
			if(array[j] > array[k])
				break;
			int temp = array[j];
			array[j] = array[k];
			array[k] = temp;
			j = k;
		}
	}
	
	public void sort(Integer[] array){
		if(null == array)
			return;
		for(int i = array.length / 2; i >= 0; i--){
			fixdown(array, array.length, i);
		}
		
		for(int i = array.length - 1; i >= 0; i--){
			int temp = array[0];
			array[0] = array[i];
			array[i] = temp;
			fixdown(array, i, 0);
		}
	}
	
	public static void main(String[] args) {
		Integer[] array = {0, 45, 654,78, 54,32, 135,54,234,6,675};
		new HeapSort().sort(array);
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i] + " ");
		}
	}
}
