package test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/** 
 * @author: yuguangyuan
 * @date 创建时间：2015年9月17日 上午9:54:20   
 */
public class SimpleDateFormatTest4 {
	
	
	public static int num = 100000;
	public static int poolSize = 4;
	public static CountDownLatch latch = new CountDownLatch(num);
	ExecutorService pool = Executors.newFixedThreadPool(poolSize);
//	ExecutorService pool = Executors.newSingleThreadExecutor();
	
	private static final String format = "yyyy-MM-dd hh:mm:ss";
	private static AtomicInteger count = new AtomicInteger(0);
	public static ThreadLocal<SimpleDateFormat> sdf = new ThreadLocal<SimpleDateFormat>(){
		protected SimpleDateFormat initialValue() {
			count.incrementAndGet();
			return new SimpleDateFormat(format);
		};
	};
	
	public class myTread implements Runnable{
				
		@Override
		public void run() {
			parseDate("2015-09-11 16:25:14".intern());
		}
		
	}
	
	public static void main(String[] args) throws InterruptedException {
		new SimpleDateFormatTest4().test();
	}
	
	public void test() throws InterruptedException {
		long start = System.currentTimeMillis();
		for (int i = 0; i < num; i++) {
			pool.execute(new myTread());
		}
		latch.await();
		System.out.println(count.get());
		System.out.println(System.currentTimeMillis() - start);
		
	}
	
	public void parseDate(String s){
		try {
			Date date = sdf.get().parse(s);
			System.out.println(date.getTime());
		} catch (ParseException e) {
			System.out.println("error");
		}finally{
			latch.countDown();
		}
	}
}
