package test;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * 采用MD5加密解密
 * @author tfq
 * @datetime 2011-10-13
 */
public class Crack {

	/***
	 * MD5加码 生成32位md5码
	 */
	public static String string2MD5(String inStr){
		MessageDigest md5 = null;
		try{
			md5 = MessageDigest.getInstance("MD5");
		}catch (Exception e){
			System.out.println(e.toString());
			e.printStackTrace();
			return "";
		}
		char[] charArray = inStr.toCharArray();
		byte[] byteArray = new byte[charArray.length];

		for (int i = 0; i < charArray.length; i++)
			byteArray[i] = (byte) charArray[i];
		byte[] md5Bytes = md5.digest(byteArray);
		StringBuffer hexValue = new StringBuffer();
		for (int i = 0; i < md5Bytes.length; i++){
			int val = ((int) md5Bytes[i]) & 0xff;
			if (val < 16)
				hexValue.append("0");
			hexValue.append(Integer.toHexString(val));
		}
		return hexValue.toString();

	}

	/**
	 * 加密解密算法 执行一次加密，两次解密
	 */ 
	public static String convertMD5(String inStr){

		char[] a = inStr.toCharArray();
		for (int i = 0; i < a.length; i++){
			a[i] = (char) (a[i] ^ 't');
		}
		String s = new String(a);
		return s;

	}

	// 测试主函数
	public static void main(String args[]) {
		int count = 1;
		for(int j = 0; j < orders.length; j++){
			for (int i = 0; i < sperators.length; i++) {
				String s = getSrc(i, j);
				System.out.println("" + count + "原始：" + s);
				
				String after = string2MD5(s);
				System.out.println("MD5后：" + after);
				System.out.println("");
				count++;
				if(after.equals(target))
					System.exit(0);
			}
		}
	}
	private static final String target = "3a49607fc51df63d82578a6346fafb77";
	private static String[] sperators = new String[]{",", ";", ":", " ", "&", "*", "#", ""};
//	private static String[] vals = new String[]{"level=10", "email=guangyuanyu@sohu-inc.com", "time=1437562593"};
	private static String[] vals = new String[]{"10", "guangyuanyu@sohu-inc.com", "1437562593"};
	
	private static int[][] orders = {
		{0, 1, 2},
		{0, 2, 1},
		{1, 2, 0},
		{1, 0, 2},
		{2, 0, 1},
		{2, 1, 0}
	};
	
	public static String getSrc(int speratorIndex, int orderIndex){
	
		String result = "";
		
		for(int i = 0; i < vals.length; i++){
			result += vals[orders[orderIndex][i]];
			if(i < vals.length - 1)
				result += sperators[speratorIndex];
		}		
		
		return result;
 	}
	
}
