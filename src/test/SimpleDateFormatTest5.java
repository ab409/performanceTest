package test;

import java.awt.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import test.SimpleDateFormatTest4.myTread;

/** 
 * @author: yuguangyuan
 * @date 创建时间：2015年9月17日 下午2:43:03   
 */
public class SimpleDateFormatTest5 {

	public static int num = 100000;
	public static int poolSize = 4;
	public static CountDownLatch latch = new CountDownLatch(num);
	ExecutorService pool = Executors.newFixedThreadPool(poolSize);
//	ExecutorService pool = Executors.newSingleThreadExecutor();
	
	private static final String format = "yyyy-MM-dd hh:mm:ss";
	private static AtomicInteger count = new AtomicInteger(0);
	public static ThreadLocal<SimpleDateFormat> sdf = new ThreadLocal<SimpleDateFormat>(){
		protected SimpleDateFormat initialValue() {
			count.incrementAndGet();
			return new SimpleDateFormat(format);
		};
	};
	
	public class myTread implements Callable<Date>{

		@Override
		public Date call() throws Exception {
			return parseDate("2015-09-11 16:25:14".intern());
		}
		
	}
	
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		new SimpleDateFormatTest5().test();
	}
	
	public void test() throws InterruptedException, ExecutionException {
		LinkedList<Future<Date>> list = new LinkedList<Future<Date>>();
		LinkedList<Date> dateList = new LinkedList<Date>();
		long start = System.currentTimeMillis();
		for (int i = 0; i < num; i++) {
			Future<Date> future = pool.submit(new myTread());
			list.add(future);
		}		
		for (Future<Date> future : list) {
			dateList.add(future.get());
		}		
		latch.await();
		long end = System.currentTimeMillis();
		for (Date date : dateList) {
			System.out.println(date.getTime());
		}
		
		System.out.println(count.get());
		System.out.println(end - start);
		System.out.println(System.currentTimeMillis() - start);
	}
	
	public Date parseDate(String s){
		Date date = null;
		try {
			date = sdf.get().parse(s);
		} catch (ParseException e) {
			System.out.println("error");
		}finally{
			latch.countDown();
		}
		return date;
	}
}
