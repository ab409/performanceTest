package test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

/** 
 * @author: yuguangyuan
 * @date 创建时间：2015年9月11日 下午4:52:35   
 */
public class SimpleDateFormatTest {

	public static int num = 100000;
	CountDownLatch latch = new CountDownLatch(num);
	
	public class myTread implements Runnable{
		
		@Override
		public void run() {
			parseDate("2015-09-11 16:25:14");
		}
		
	}
	
	public static void main(String[] args) throws InterruptedException {
		new SimpleDateFormatTest().test();
	}
	
	public void test() throws InterruptedException {
		long start = System.currentTimeMillis();
		for (int i = 0; i < num; i++) {
			new Thread(new myTread()).start();
		}
		latch.await();
		System.out.println(System.currentTimeMillis() - start);
	}
	
	public void parseDate(String s){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		try {
			Date date = sdf.parse(s);
//			System.out.println(date.getTime());
		} catch (ParseException e) {
			System.out.println("error");
		}finally{
			latch.countDown();
		}
	}
	
}
