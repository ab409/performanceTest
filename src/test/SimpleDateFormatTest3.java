package test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

/** 
 * @author: yuguangyuan
 * @date 创建时间：2015年9月17日 上午9:48:11   
 */
public class SimpleDateFormatTest3 {
	
	public static int num = 100000;
	public static CountDownLatch latch = new CountDownLatch(num);
		
	public static class myTread implements Runnable{
		
		public static ThreadLocal<SimpleDateFormat> sdf = new ThreadLocal<SimpleDateFormat>(){
			protected SimpleDateFormat initialValue() {
				return new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			};
		};
		@Override
		public void run() {
			try {
				Date date = sdf.get().parse("2015-09-11 16:25:14");
				System.out.println(date.getTime());
			} catch (ParseException e) {
				System.out.println("error");
			}finally{
				latch.countDown();
			}
		}
		
	}
	
	public static void main(String[] args) throws InterruptedException {
		new SimpleDateFormatTest3().test();
	}
	
	public void test() throws InterruptedException {
		long start = System.currentTimeMillis();
		for (int i = 0; i < num; i++) {
			new Thread(new myTread()).start();
		}
		latch.await();
		System.out.println(System.currentTimeMillis() - start);
	}
}
