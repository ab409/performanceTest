package test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

/** 
 * @author: yuguangyuan
 * @date 创建时间：2015年9月11日 下午4:52:53   
 */
public class SimpleDateFormatTest2 {
	
	public static int num = 100000;
	CountDownLatch latch = new CountDownLatch(num);
	public static AtomicInteger count = new AtomicInteger(0);
	public static ThreadLocal<SimpleDateFormat> sdf = new ThreadLocal<SimpleDateFormat>(){
		protected SimpleDateFormat initialValue() {
			count.incrementAndGet();
			return new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		};
	};
	public class myTread implements Runnable{
		
		@Override
		public void run() {
			parseDate("2015-09-11 16:25:14");
		}
		
	}
	
	public static void main(String[] args) throws InterruptedException {
		new SimpleDateFormatTest2().test();
	}
	
	public void test() throws InterruptedException {
		long start = System.currentTimeMillis();
		for (int i = 0; i < num; i++) {
			new Thread(new myTread()).start();
		}
		latch.await();
		System.out.println(count.get());
		System.out.println(System.currentTimeMillis() - start);
	}

	public void parseDate(String s){
		try {
			Date date = sdf.get().parse(s);
//			System.out.println(date.getTime());
		} catch (ParseException e) {
			System.out.println("error");
		}finally{
			latch.countDown();
		}
	}
}
