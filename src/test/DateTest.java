package test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/** 
 * @author: yuguangyuan
 * @date 创建时间：2015年9月11日 下午3:22:29   
 */
public class DateTest {
	public static void main(String[] args) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date dateA = sdf.parse("2015-09-11 10:01:02");
		Date dateB = sdf.parse("2015-09-10 23:10:10");
		System.out.println(new DateTest().isSameDay(dateA, dateB));
	}
	
	private boolean isSameDay(Date dateA, Date dateB){
		if(null == dateA || null == dateB)
			return false;
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(dateA);
		cal2.setTime(dateB);
		
		int year1 = cal1.get(Calendar.YEAR);
		int year2 = cal2.get(Calendar.YEAR);
		
		int day1 = cal1.get(Calendar.DAY_OF_YEAR);
		int day2 = cal2.get(Calendar.DAY_OF_YEAR);
		
		return year1 == year2 && day1 == day2;
//		boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
//		                  cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
//		return sameDay;
	}
}
